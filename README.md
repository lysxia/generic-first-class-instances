# A Deriving Library

([Read this README rendered on Gitlab.](https://gitlab.com/lysxia/ad-lib#a-deriving-library))

Deriving as a library, using generic first class instances.

*first-class-instances* provides an alternative to
the `instance` syntax for defining instances and manipulating them.
*ad-lib* implements "deriving" features for *first-class-instances*,
using *kind-generics*.

---

**Table of contents**

[TOC]

---

# First class instances

Brief recap of how to use *first-class-instances*.

1. The `mkDict` command associates every class `C` to a data type `Dict (C _)`.
2. The `instanceDict` command registers a given dictionary `d :: Dict (C T)` as an instance of `C`.

Example:

```haskell
{-# LANGUAGE TemplateHaskell, TypeFamilies #-}
import FCI (Dict, mkDict, instanceDict)

-- Example class
class Magma a where
  mix :: a -> a -> a

-- (1) Declare dictionary type for the class Magma
mkDict ''Magma

-- Example dictionary
myMagma :: Dict (Magma Int)
myMagma = Magma { _mix = (+) }

-- (2) Register dictionary myMagma as an instance of Magma
instanceDict [| myMagma :: Dict (Magma Int) |]
```

# Deriving as a library: *ad-lib*

Deriving strategies are implementations of an overloaded dictionary `derive`.
It belongs to the `Derivable` class, indexed by an identifier `s` for deriving strategies.

```haskell
class Derivable s c where
  derive :: Dist c
```

Given a deriving strategy, such as `Via A`, and provided it is implemented by to
the given class and type, we can derive an instance `C B` as follows:

```haskell
instanceDict [| derive @(Via A) :: Dict (C B) |]
```

Now that classes are data types, they can be instances of `GenericK`.
This lets us **generically derive deriving strategies**,
as demonstrated below in subsections of [*deriving via*](#extending-deriving-via)
and [*generic deriving*](#generic-deriving-of-generic-deriving).

## Deriving via

```haskell
data Via a
```

This generalizes GHC's `via` strategy (see section [Extending *deriving via* further](#extending-deriving-via-further)).

The above example:

```haskell
instanceDict [| derive @(Via A) :: Dict (C B) |]
```

... is equivalent to these old-school `deriving` constructs:

```haskell
data B = ...
  deriving C via A

-- or --

deriving via A instance C B
```

### Newtype deriving

Newtype deriving is the special case of `Via` where the `Via` type is
the underlying type of the newtype, with the main benefit of not having
to spell out that type.

`deriveNewtype` requires the newtype constructor to infer the underlying type.

```haskell
newtype T = T Int

instanceDict [| deriveNewtype T :: Dict (Eq T) |]
```

### Deriving *deriving via*

*Deriving via* is backed by the class `CoerceDict`, which converts between dictionaries.

```haskell
class CoerceDict c d where
  coerceDict :: Dict c -> Dict d
```

Implementing `CoerceDict` for a class `C` enables using *deriving via* for that class.
`CoerceDict` instances can be derived generically;
in other words, we can **generically derive *deriving via***.

First, the class's dictionary type must implement `GenericK`.

```haskell
-- Prerequisite for class C
deriveGenericK 'C  -- C's dictionary type must be an instance of GenericK
```

The `CoerceDict` instance can then be derived generically.

```
-- Generic deriving of deriving via for class C
instanceDict [| derive @Generically :: Coercible a b => Dict (CoerceDict (C a) (C b)) |]
```

Generally, a `Coercible` constraint is required (`Coercible1` for higher-kinded
classes, such as `Functor`), as well as any superclasses.

Most classes from *base* have generically derived *deriving via*,
providing the same coverage as GHC's `DerivingVia` extension.

```haskell
instanceDict [| derive @Generically ::  Coercible a b                  => Dict (CoerceDict (Eq a) (Eq b)) |]
instanceDict [| derive @Generically :: (Coercible a b, Eq b)           => Dict (CoerceDict (Ord a) (Ord b)) |]
instanceDict [| derive @Generically ::  Coercible a b                  => Dict (CoerceDict (Semigroup a) (Semigroup b)) |]
instanceDict [| derive @Generically :: (Coercible a b, Semigroup b)    => Dict (CoerceDict (Monoid a) (Monoid b)) |]
instanceDict [| derive @Generically ::  Coercible1 f g                 => Dict (CoerceDict (Functor f) (Functor g)) |]
instanceDict [| derive @Generically :: (Coercible1 f g, Functor g)     => Dict (CoerceDict (Applicative f) (Applicative g)) |]
instanceDict [| derive @Generically :: (Coercible1 f g, Applicative g) => Dict (CoerceDict (Monad f) (Monad g)) |]
instanceDict [| derive @Generically ::  Coercible1 f g                 => Dict (CoerceDict (Foldable f) (Foldable g)) |]
```

### Extending *deriving via*

The generic implementation of *deriving via* does not apply to all classes.
A common example is the `Traversable` class.
[See also this blogpost by Ryan Scott.](https://ryanglscott.github.io/2018/06/22/quantifiedconstraints-and-the-trouble-with-traversable/)

*ad-lib*'s `derive @(Via _)` can be extended to handle such cases,
by manually writing an instance of `CoerceDict` for the relevant class.

```haskell
coerceDictTraversable :: forall f g. (Coercible1 f g, Functor g, Foldable g) => Dict (CoerceDict (Traversable f) (Traversable g))
coerceDictTraversable = CoerceDict (\Traversable{..} -> Traversable
  { _Functor = dict
  , _Foldable = dict
  , _traverse  = \f -> fmap coerce . _traverse  f . coerce
  , _mapM      = \f -> fmap coerce . _mapM      f . coerce
  , _sequenceA = fmap coerce . _sequenceA . (coerce :: g x -> f x)
  , _sequence  = fmap coerce . _sequence  . (coerce :: g x -> f x) })

instanceDict [| coerceDictTraversable :: (Coercible1 f g, Functor g, Foldable g) => Dict (CoerceDict (Traversable f) (Traversable g)) |]
```

Voilà. *Deriving via* now works for `Traversable`.

```haskell
newtype List a = List [a]

instanceDict [| derive @(Via []) :: Dict (Functor List) |]
instanceDict [| derive @(Via []) :: Dict (Foldable List) |]
instanceDict [| derive @(Via []) :: Dict (Traversable List) |]
```

Note that since this definition necessarily diverges from the simple scheme of
"apply `coerce` to every field", there may be a run-time cost to such instances.

## Generic deriving

```haskell
data Generically
```

Generic deriving generalizes GHC's `stock` strategy, generating an
implementation from a data type's structure.

```haskell
instanceDict [| derive @Generically :: Dict (Eq T) |]
instanceDict [| derive @Generically :: Dict (Ord T) |]
```

Whereas `stock` is built into the compiler, generic deriving can be
extended to other type classes, such as `Semigroup` and `Monoid`:

```haskell
instanceDict [| derive @Generically :: Dict (Semigroup T) |]
instanceDict [| derive @Generically :: Dict (Monoid T) |]
instanceDict [| derive @Generically :: Dict (Applicative F) |]
```

Note that generic deriving strategies may be partial, applying only
to some data types but not others. A common restriction is for data
types to be be products (*i.e.*, with exactly one constructor),
as required by `Semigroup` and `Monoid` above.

### Generic deriving of generic deriving

Generic deriving for a class `C` usually relies on an auxiliary class `GC`
indexed by generic representations, so that `C a` can be derived generically if
there is an instance of `GC (RepK a)`, or something along those lines.

For some classes `C`, the auxiliary class `GC` may be `C` itself.
Among standard classes, this is the case for `Eq`, `Ord`, `Semigroup`,
`Monoid`, `Functor`, `Applicative`, `Monad`, `Foldable`, `Traversable`.

In that case, generic deriving can be generically derived, following
the recipe "use `toK` and `fromK` to transform `Dict (C (RepK a))` into
`Dict (C a)`".

```haskell
-- Prerequisite for class C
deriveCoerce 'C  -- C's dictionary type must be an instance of GenericK

-- Generic deriving of generic deriving for C
instanceDict [| derive @Generically :: Structurally C a => Dict (Derivable Generically (C a)) |]
```

Notable classes excluded from this scheme are `Show` and `Read`.
They depend on nominal information (names of constructors) instead of
structural information only (sum-of-product structure).

## Default deriving

```haskell
data Default
```

A class may have a default implementation.
An example is the `Exception` class.

```haskell
instanceDict [| derive @Default :: Dict (Exception T) |]
```

Similarly to other strategies, default implementations are declared as
instances of `Derivable Default`:

```haskell
defaultException :: ... => Dict (Exception e)

instanceDict [| Derivable defaultException :: ... => Dict (Derivable Default (Exception e)) |]
```

## Partial default implementations

Other classes may be implemented using a subset of their methods.
For example, we can implement `Ord` by only providing one method `compare`,
and other methods, like `(<=)`, will be implicitly defined using it.

A class `C` may have a partial default implementation by implementing
`PartialDefault` providing a dictionary `partialDefault` for instances of `C`
to complete with a record update.

```haskell
data T                   -- Declare data type.
(.<=) :: T -> T -> Bool  -- Implement (<=).

-- Derive the other Ord methods.
instanceDict [| partialDefault { (|<=) = (.<=) } :: Dict (Ord T) |]
```

## Ad-libbing with derived dictionaries

Since a dictionary is a value, like `derive` or `partialDefault`, we can tweak
it to our leisure before making it an instance. Unlike old-school
`deriving`, who is more of a "take it or leave it" kind of guy.

Example: there's a data type `T` with many constructors, and we want to
ignore the field of one of them, treating them as always equal.
We can use the stock/generically derived dictionary as a starting point
and simply override the relevant case.

```haskell
data T = A | B | C | D IgnoreMe

(.==) :: T -> T -> Bool
D _ .== D _ = True
x .== y = x |== y
  where
    Eq{(|==) = (|==)} = derive @Generically

(.<=) :: T -> T -> Bool
D _ .<= D _ = True
x .<= y = x |<= y
  where
    Ord{(|<=) = (|<=)} = derive @Generically

instanceDict [| partialDefault { (|==) = (.==) } :: Dict (Eq T) |]
instanceDict [| partialDefault { (|<=) = (.<=) } :: Dict (Ord T) |]
```

Note: using this strategy, `IgnoreMe` must be an instance of `Ord`, even though
that is ultimately ignored. Another approach is to modify the generic representation
of the type `T` before calling `derive @Generically`.
See also [*generic-data-surgery*](https://hackage.haskell.org/package/generic-data-surgery).
