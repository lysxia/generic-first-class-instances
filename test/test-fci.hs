{-# LANGUAGE TemplateHaskell, TypeFamilies #-}
import FCI (Dict, mkDict, instanceDict)

-- Example class
class Magma a where
  mix :: a -> a -> a

-- (1) Declare dictionary type for the class Magma
mkDict ''Magma

-- Example dictionary
myMagma :: Dict (Magma Int)
myMagma = Magma { _mix = (+) }

-- (2) Register dictionary myMagma as an instance of Magma
instanceDict [| myMagma :: Dict (Magma Int) |]

main :: IO ()
main = pure ()
