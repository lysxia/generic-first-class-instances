{-# LANGUAGE
  DataKinds,
  KindSignatures,
  ScopedTypeVariables,
  TemplateHaskell,
  TypeApplications,
  TypeFamilies #-}

import Adlib
import Adlib.Instances ()
import Adlib.Lift

import FCI (Dict, instanceDict)
import FCI.Base

import Generics.Kind.TH

import Test.Tasty
import Test.Tasty.HUnit

import Control.Exception
import Control.Monad.Trans.Reader
import Control.Monad.Trans.State
import Data.Coerce
import Data.Functor.Identity
import Data.Functor.Compose (Compose(..))
import Data.Functor.Product (Product(..))
import Data.Monoid (Sum(..))

-- Deriving via
newtype T = T Int
  deriving Show

instanceDict [| derive @(Via Int) :: Dict (Eq T) |]
instanceDict [| derive @(Via Int) :: Dict (Ord T) |]
instanceDict [| derive @(Via (Sum Int)) :: Dict (Semigroup T) |]
instanceDict [| derive @(Via (Sum Int)) :: Dict (Monoid T) |]

-- Newtype deriving
newtype NT = NT Int

instanceDict [| deriveNewtype NT :: Dict (Eq NT) |]

-- Deriving via for Traversable
newtype List a = List [a]
  deriving Show

instanceDict [| derive @(Via [a]) :: forall a. Eq a => Dict (Eq (List a)) |]
instanceDict [| derive @(Via []) :: Dict (Functor List) |]
instanceDict [| derive @(Via []) :: Dict (Foldable List) |]
instanceDict [| derive @(Via []) :: Dict (Traversable List) |]

-- (M r s) is not coercible to (StateT s (Reader r)),
-- but (M r s a) is coercible to (StateT s (Reader r) a)
-- This requires a little bit of sophistication in the implementation of deriving via.
newtype M r s a = M { unM :: s -> r -> (a, s) }

instanceDict [| derive @(Via (StateT s (Reader r))) :: forall r s. Dict (Functor (M r s)) |]
instanceDict [| derive @(Via (StateT s (Reader r))) :: forall r s. Dict (Applicative (M r s)) |]
instanceDict [| derive @(Via (StateT s (Reader r))) :: forall r s. Dict (Monad (M r s)) |]

-- Default deriving Exception
data E = E
  deriving Show

deriveGenericK ''E

instanceDict [| derive @Generically :: Dict (Eq E) |]
instanceDict [| derive @Default :: Dict (Exception E) |]

-- README example: partialDefault
data N = Z | S N

(.==) :: N -> N -> Bool
Z .== Z = True
S m .== S n = m .== n
_ .== _ = False

(.<=) :: N -> N -> Bool
Z .<= _ = True
S _ .<= Z = False
S m .<= S n = m .<= n

instanceDict [| partialDefault { (|==) = (.==) } :: Dict (Eq N) |]
instanceDict [| partialDefault { (|<=) = (.<=) } :: Dict (Ord N) |]

-- README example: using derive outside of an instance
data Four = A | B | C | D Int

deriveGenericK ''Four

(#==) :: Four -> Four -> Bool
D _ #== D _ = True
x #== y = x |== y
  where
    Eq{(|==) = (|==)} = derive @Generically

(#<=) :: Four -> Four -> Bool
D _ #<= D _ = True
x #<= y = x |<= y
  where
    Ord{(|<=) = (|<=)} = derive @Generically

instanceDict [| partialDefault { (|==) = (#==) } :: Dict (Eq Four) |]
instanceDict [| partialDefault { (|<=) = (#<=) } :: Dict (Ord Four) |]

tests :: TestTree
tests = testGroup "Tests"
  [ testCase "Eq T" $ (T 0 == T 1) @?= False
  , testCase "Eq NT" $ (NT 0 == NT 1) @?= False
  , testCase "Traverse List" $ traverse Just (List [1,2,3 :: Int]) @?= Just (List [1,2,3])
  , testCase "Functor M" $ unM (fmap (+ 1) (M (\s r -> (r, s)))) (0 :: Int) (10 :: Int) @?= (11, 0)
  , testCase "Ord N" $ (S (S Z) >= S Z) @?= True
  , testCase "Ord Four" $ (D 1 <= D 0) @?= True
  , testCase "Exception E" $ do
      e <- try (throw E :: IO ())
      e @?= Left E
  , liftTests
  ]

liftedSemigroup :: forall a b. (Semigroup a, Semigroup b) => Dict (Semigroup (a, b))
liftedSemigroup = glifted

liftedSemigroup3 :: forall a b c. (Semigroup a, Semigroup b, Semigroup c) => Dict (Semigroup (a, b, c))
liftedSemigroup3 = glifted

liftedFunctor :: forall f g. (Functor f, Functor g) => Dict (Functor (Product f g))
liftedFunctor = glifted1

liftedApplicative :: forall f g. (Applicative f, Applicative g) => Dict (Applicative (Product f g))
liftedApplicative = glifted1

liftedMonad :: forall f g. (Monad f, Monad g) => Dict (Monad (Product f g))
liftedMonad = glifted1

liftTests :: TestTree
liftTests = testGroup "Lift"
  [ testCase "Semigroup" $
      let Semigroup{ (|<>) = (%) } = liftedSemigroup in
      (("abc", "ghi") % ("def", "jkl")) @?= ("abcdef", "ghijkl")
  ]

main :: IO ()
main = defaultMain tests
