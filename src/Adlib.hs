-- | A deriving library.

module Adlib
  ( -- * Deriving
    Derivable(..)

    -- ** Deriving via
  , Via

    -- *** Newtype deriving
  , deriveNewtype

    -- *** Extending deriving via
  , CoerceDict(..)
  , Coercible1

    -- ** Deriving generically
  , Generically

    -- *** Structural generic deriving
  , Structurally

    -- ** Default deriving
  , Default

    -- * Default methods
  , PartialDefault(..)

    -- * First-class instances representation

    -- $dogfood
  , DictDerivable(..)
  , DictCoerceDict(..)
  , DictPartialDefault(..)
  ) where

import Adlib.Strategies
import Adlib.CoerceDict
import Adlib.Default ()
import Adlib.Generically (Structurally)

-- $dogfood
-- Here we eat our own dogfood and generate
-- dictionary types for the classes of this library
-- so we can create instances using 'FCI.instanceDict'.
