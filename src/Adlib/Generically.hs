{-# LANGUAGE
  AllowAmbiguousTypes,
  BlockArguments,
  ConstraintKinds,
  DataKinds,
  FlexibleContexts,
  FlexibleInstances,
  ImpredicativeTypes,
  QuantifiedConstraints,
  PolyKinds,
  MultiParamTypeClasses,
  ScopedTypeVariables,
  TemplateHaskell,
  TypeApplications,
  TypeOperators,
  UndecidableInstances,
  UndecidableSuperClasses #-}
module Adlib.Generically where

import Adlib.Strategies
import Adlib.Instances
import Adlib.Default
import Adlib.MapIso
import Adlib.Internal (with)

import FCI (Dict, dict, instanceDict)

import Generics.Kind (GenericK(..), LoT(..))

import Data.Function (fix, on)

-- | A generic type @a@ is structurally an instance of @c@ when @RepK@ is an
-- instance of @c@.
type Structurally c a = (GenericK a, forall x. AppRepK c a x)

class    c (RepK a x) => AppRepK c a x
instance c (RepK a x) => AppRepK c a x

instance (MapIso f, HasDict1 f c, Structurally c a) =>
  Derivable Generically (Derivable Generically (c a)) where
  derive = Derivable (with @(HasDictAt1 f c a) (
                      with @(HasDictAt1 f c (RepK a 'LoT0)) (
                      with @(AppRepK c a 'LoT0) (
                        (mapIso @f (genericIso @a @LoT0) (dict @(c (RepK a LoT0))))))))

instanceDict [| derive @Generically :: Structurally Eq a => Dict (Derivable Generically (Eq a)) |]
instanceDict [| derive @Generically :: Structurally Ord a => Dict (Derivable Generically (Ord a)) |]
instanceDict [| derive @Generically :: Structurally Semigroup a => Dict (Derivable Generically (Semigroup a)) |]
instanceDict [| derive @Generically :: Structurally Monoid a => Dict (Derivable Generically (Monoid a)) |]
