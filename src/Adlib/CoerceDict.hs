{-# LANGUAGE
  ConstraintKinds,
  FlexibleContexts,
  FlexibleInstances,
  InstanceSigs,
  KindSignatures,
  MultiParamTypeClasses,
  QuantifiedConstraints,
  RankNTypes,
  RecordWildCards,
  ScopedTypeVariables,
  TypeApplications,
  TemplateHaskell,
  UndecidableInstances #-}

module Adlib.CoerceDict
  ( CoerceDict(..)
  , DictCoerceDict(..)
  , Coercible1
  ) where

import Adlib.CoerceDict.Class
import Adlib.CoerceDict.Generic
import Adlib.Strategies
import Adlib.Instances

import FCI (Dict, dict, instanceDict)
import FCI.Base

import Data.Coerce
import Data.Kind (Constraint)

instance GCoerceDict c d => Derivable Generically (CoerceDict c d) where
  derive = gCoerceDict

-- | Coercion between parameterized types. This is more general than Coercible f g.
class (forall a. Coercible (g a) (f a), forall a. Coercible (f a) (g a)) => Coercible1 f g
instance (forall a. Coercible (g a) (f a), forall a. Coercible (f a) (g a)) => Coercible1 f g

instanceDict [| derive @Generically ::  Coercible a b                  => Dict (CoerceDict (Eq a) (Eq b)) |]
instanceDict [| derive @Generically :: (Coercible a b, Eq b)           => Dict (CoerceDict (Ord a) (Ord b)) |]
instanceDict [| derive @Generically ::  Coercible a b                  => Dict (CoerceDict (Semigroup a) (Semigroup b)) |]
instanceDict [| derive @Generically :: (Coercible a b, Semigroup b)    => Dict (CoerceDict (Monoid a) (Monoid b)) |]
instanceDict [| derive @Generically ::  Coercible1 f g                 => Dict (CoerceDict (Functor f) (Functor g)) |]
instanceDict [| derive @Generically :: (Coercible1 f g, Functor g)     => Dict (CoerceDict (Applicative f) (Applicative g)) |]
instanceDict [| derive @Generically :: (Coercible1 f g, Applicative g) => Dict (CoerceDict (Monad f) (Monad g)) |]
instanceDict [| derive @Generically ::  Coercible1 f g                 => Dict (CoerceDict (Foldable f) (Foldable g)) |]

coerceDictTraversable :: forall f g. (Coercible1 f g, Functor g, Foldable g) => Dict (CoerceDict (Traversable f) (Traversable g))
coerceDictTraversable = CoerceDict (\Traversable{..} -> Traversable
  { _Functor = dict
  , _Foldable = dict
  , _traverse  = \f -> fmap coerce . _traverse  f . coerce
  , _mapM      = \f -> fmap coerce . _mapM      f . coerce
  , _sequenceA = fmap coerce . _sequenceA . (coerce :: g x -> f x)
  , _sequence  = fmap coerce . _sequence  . (coerce :: g x -> f x) })

instanceDict [| coerceDictTraversable :: (Coercible1 f g, Functor g, Foldable g) => Dict (CoerceDict (Traversable f) (Traversable g)) |]
