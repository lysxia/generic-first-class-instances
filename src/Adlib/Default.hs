{-# LANGUAGE
  CPP,
  DisambiguateRecordFields,
  MultiParamTypeClasses,
  ScopedTypeVariables,
  TemplateHaskell,
  TypeApplications #-}

module Adlib.Default where

import Adlib.Strategies
import Adlib.Instances

import FCI (Dict, dict, instanceDict)
import FCI.Base

import Control.Exception (SomeException(..), Exception(..))
import Data.Typeable (Typeable, cast)

-- | This type looks funny because the default exception instance
-- is actually recursive.
defaultException :: forall e. Exception e => Dict (Exception e)
defaultException = Exception
  { _Typeable = dict @(Typeable e)
  , _Show = dict @(Show e)
  , _toException = SomeException
  , _fromException = \(SomeException e) -> cast e
  , _displayException = show
#if __GLASGOW_HASKELL__ >= 910
  , _backtraceDesired = \_ -> True
#endif
  }

instanceDict [| Derivable defaultException
  :: Exception e => Dict (Derivable Default (Exception e)) |]

instance Eq a => PartialDefault (Eq a) where
  partialDefault = Eq
    { (|==) = \x y -> not (x /= y)
    , (|/=) = \x y -> not (x == y) }

instance Ord a => PartialDefault (Ord a) where
  partialDefault = Ord
    { _Eq = dict @(Eq a)
    , _compare = \x y -> if x == y then EQ else if x <= y then LT else GT
    , (|<)  = \x y -> compare x y == LT
    , (|<=) = \x y -> compare x y /= GT
    , (|>)  = \x y -> compare x y == GT
    , (|>=) = \x y -> compare x y /= LT
    , _max  = \x y -> if x <= y then y else x
    , _min  = \x y -> if x <= y then x else y
    }
