{-# LANGUAGE
  ConstraintKinds,
  DataKinds,
  KindSignatures,
  PolyKinds,
  RankNTypes,
  TemplateHaskell,
  TypeFamilies #-}
-- PolyKinds needed by GHC 9.0

module Adlib.Internal where

import Fcf.Family.TH (fcfify, familyName)
import FCI.Internal (Dict)

-- | Needed by GHC 9.0, but not on GHC 9.2 or after.
-- Would be cool to know why.
--
-- https://mail.haskell.org/pipermail/haskell-cafe/2022-September/135571.html
with :: forall c t. (c => t) -> (c => t)
with x = x


fcfify ''Dict

-- | 'Fcf.Family.Name' of 'FCI.Dict'.
-- (Note this is actually the internal name, which is exposed in 'RepK' instances.)
type DictName = $(pure (familyName ''Dict))
