{-# LANGUAGE
  AllowAmbiguousTypes,
  ConstraintKinds,
  DataKinds,
  EmptyCase,
  FlexibleContexts,
  FlexibleInstances,
  FunctionalDependencies,
  GADTs,
  KindSignatures,
  LambdaCase,
  MultiParamTypeClasses,
  PolyKinds,
  QuantifiedConstraints,
  RankNTypes,
  ScopedTypeVariables,
  TemplateHaskell,
  TypeApplications,
  TypeFamilies,
  TypeOperators,
  UndecidableInstances,
  UndecidableSuperClasses #-}
-- | Derive instance for products
module Adlib.Lift.Core where

import Data.Kind (Type, Constraint)
import Data.Proxy
import Data.Functor.Product

import qualified Data.Constraint as C

import Fcf.Core (Exp, Eval)
import Fcf.Combinators
import FCI

import Adlib.Internal (with)

-- * Indices

data family Index (i :: ix)

type family (!!) (a :: k) (i :: ix) :: k

-- Nat

data Z
data S n = SZ | SS n

type S1 = S Z
type S2 = S S1
type S3 = S S2

data instance Index (_ :: Z)
data instance Index (_ :: S n) where
  IZ :: Index 'SZ
  IS :: Index i -> Index ('SS i)

type instance t a !! SZ = a
type instance t a b !! SS SZ = a
type instance t a b c !! SS (SS SZ) = a

-- Shift

newtype Shift n = Shift n

data instance Index (_ :: Shift n) where
  IShift :: Index i -> Index ('Shift i)

type instance t a !! Shift n = (t !! n) a

-- Functions

data FunArg (a :: Type) = Arg

newtype instance Index (i :: FunArg a) = IArg a

type instance (a -> b) !! (_ :: FunArg a) = b

-- * Lift

type LiftDict ix c a = (forall (i :: ix). Index i -> Dict (c (a !! i))) -> Dict (c a)

type Lift ix a = (forall (i :: ix). Index i -> a !! i) -> a
type Project ix a = forall (i :: ix). a -> Index i -> a !! i

class TheLift ix a | a -> ix where
  theLift :: Lift ix a

class TheProject ix a | a -> ix where
  theProject :: Project ix a

type Lift1 ix f = forall x. (forall (i :: ix). Index i -> (f !! i) x) -> f x
type Project1 ix f = forall x (i :: ix). f x -> Index i -> (f !! i) x

class TheLift1 ix f | f -> ix where
  theLift1 :: Lift1 ix f

class TheProject1 ix f | f -> ix where
  theProject1 :: Project1 ix f

-- * Lift instances

instance TheLift Z () where
  theLift _ = ()

instance TheLift S2 (a, b) where
  theLift p = (p (IS IZ), p IZ)

instance TheLift S3 (a, b, c) where
  theLift p = (p (IS (IS IZ)), p (IS IZ), p IZ)

instance TheLift (FunArg a) (a -> b) where
  theLift f x = f (IArg x)

instance TheProject Z () where
  theProject _ = \case {}

instance TheProject S2 (a, b) where
  theProject (a, b) = \case
    IS IZ -> a
    IZ -> b

instance TheProject S3 (a, b, c) where
  theProject (a, b, c) = \case
    IS (IS IZ) -> a
    IS IZ -> b
    IZ -> c

instance TheProject (FunArg a) (a -> b) where
  theProject f (IArg x) = f x

instance TheLift1 Z Proxy where
  theLift1 _ = Proxy

instance TheLift1 S2 (Product f g) where
  theLift1 p = Pair (p (IS IZ)) (p IZ)

instance TheProject1 S2 (Product f g) where
  theProject1 (Pair f g) = \case
    IS IZ -> f
    IZ -> g

-- * Enumerating constraints

class ForEach (ix :: Type) (c :: ix -> Exp Constraint) where
  point :: forall (i :: ix). Index i -> C.Dict (Eval (c i))

instance ForEach Z c where
  point x = case x of

instance (Eval (c SZ), ForEach n (c <=< Pure1 SS)) => ForEach (S n) c where
  point IZ = C.Dict
  point (IS i) = point @n @(c <=< Pure1 SS) i

instance ForEach n (c <=< Pure1 'Shift) => ForEach (Shift n) c where
  point (IShift i) = point @n @(c <=< Pure1 'Shift) i

class Eval c => CEval (c :: Exp Constraint)
instance Eval c => CEval (c :: Exp Constraint)

instance (forall i. CEval (c i)) => ForEach (FunArg a) c where
  point (_ :: Index i) = with @(CEval (c i)) C.Dict

data Indexed (c :: k -> Constraint) (a :: k) (i :: ix) :: Exp Constraint
type instance Eval (Indexed c a i) = c (a !! i)

fromCDict :: C.Dict c -> Dict c
fromCDict C.Dict = dict

elevate :: forall ix c a. ForEach ix (Indexed c a) => LiftDict ix c a -> Dict (c a)
elevate f = f (fromCDict . point @ix @(Indexed c a))

class Given s a where
  given :: a

mkDict ''Given
