{-# LANGUAGE
  AllowAmbiguousTypes,
  DataKinds,
  FlexibleContexts,
  FlexibleInstances,
  GADTs,
  ImpredicativeTypes,
  MultiParamTypeClasses,
  QuantifiedConstraints,
  ScopedTypeVariables,
  RankNTypes,
  PolyKinds,
  TypeApplications,
  TypeFamilies,
  TypeOperators,
  UndecidableInstances #-}
module Adlib.Lift.Generic where

import Data.Kind
import Data.Type.Equality
import Data.Functor.Product
import Data.Functor.Sum
import qualified Data.Constraint as C

import Fcf.Core (Exp, Eval)

import FCI
import Generics.Kind
  ( LoT(..), (:*:)(..), M1(..), U1(..), Field(..), Atom(..), SuchThatI(..)
  , Interpret, TyVar(..), InterpretVar, WrappedI(..), fromWrappedI, toWrappedI )
import qualified Generics.Kind as K

import Adlib.Lift.Core
import Adlib.Internal (with)

data Choice k = Constant k | Choose k

type family (!??) (x :: Choice k) (i :: ix) :: k where
  Constant a !?? i = a
  Choose a !?? i = a !! i

type family (!?) (xs :: LoT k) (i :: ix) :: LoT l where
  (x :&&: xs) !? i = (x !?? i) :&&: (xs !? i)
  LoT0 !? i = LoT0

type family BaseV (x :: Choice k) :: k where
  BaseV (Constant a) = a
  BaseV (Choose a) = a

type family Base (xs :: LoT k) :: LoT l where
  Base (x :&&: xs) = BaseV x :&&: Base xs
  Base LoT0 = LoT0

-- To make this customizable, all these classes are parameterized by a universally quantified @s@,
-- which is a "fresh scope" which can be initialized with some first-class instances.
class GLift s ix f xs where
  glift :: (forall (i :: ix). Index i -> f (xs !? i)) -> f (Base xs)

instance GLift s ix f xs => GLift s ix (M1 i c f) xs where
  glift p = M1 (glift @s @ix @f @xs (unM1 . p))

instance (GLift s ix f xs, GLift s ix g xs) => GLift s ix (f :*: g) xs where
  glift p = glift @s @ix @f @xs (proj1 . p) :*: glift @s @ix @g @xs (proj2 . p)
    where
      proj1 (x :*: _) = x
      proj2 (_ :*: y) = y

instance GLift s ix U1 xs where
  glift _ = U1

instance ALift s ix t xs => GLift s ix (Field t) xs where
  glift p = Field (alift @s @ix @t @xs (unField . p))

class ALift s ix t xs where
  alift :: (forall (i :: ix). Index i -> Interpret t (xs !? i)) -> Interpret t (Base xs)

wrappedAlift :: forall s ix t xs. ALift s ix t xs =>
  (forall (i :: ix). Index i -> WrappedI t (xs !? i)) -> WrappedI t (Base xs)
wrappedAlift p = WrapI (alift @s @ix @t @xs (unwrapI . p))

instance (forall (x :: d1). ALift s ix t (Constant x :&&: xs))
  => ALift s ix ('ForAll (t :: Atom (d1 -> d) Type)) xs where
  alift p = fromWrappedI (wrappedAlift @s @ix @t @(Constant x :&&: xs) (toWrappedI . p)
    :: forall x. WrappedI t (x :&&: Base xs))

data Indexed' (u :: Atom k Constraint) (xs :: LoT l) (i :: ix) :: Exp Constraint
type instance Eval (Indexed' u xs i) = Interpret u (xs !? i)

instance
  ( Interpret u (Base xs) => ALift s ix t xs
  , Interpret u (Base xs) => ForEach ix (Indexed' u xs))
  => ALift s ix (u :=>>: t) xs where
  alift p = SuchThatI (alift @s @ix @t @xs (\i ->
    case point @ix @(Indexed' u xs) i of C.Dict -> unSuchThatI (p i)))

instance ALift s ix t xs => ALift s ix (Kon ((->) a) :@: t) xs where
  alift p a = alift @s @ix @t @xs (\i -> p i a)

instance ALift s ix (Var v) xs => ALift s ix (Var (VS v :: TyVar (a -> k) Type)) ((x :: Choice a) :&&: xs) where
  alift = alift @s @ix @(Var v) @xs

instance Given s (WLift ix a)
  => ALift s ix (Var (VZ :: TyVar (Type -> k) Type)) (Choose a :&&: xs) where
  alift = unWLift (given @s @(WLift ix a))

class AProject s ix t xs where
  aproject :: forall (i :: ix). Interpret t (Base xs) -> Index i -> Interpret t (xs !? i)

instance (AProject s ix u xs, ALift s ix t xs) => ALift s ix ((Kon (->) :@: u) :@: t) xs where
  alift p u = alift @s @ix @t @xs (\i -> p i (aproject @s @ix @u @xs u i))

class (Interpret u (Base xs) ~ Interpret u (xs !? i)) => ClosedInterpret u xs i
instance (Interpret u (Base xs) ~ Interpret u (xs !? i)) => ClosedInterpret u xs i

instance
    ( forall (i :: ix). ClosedInterpret u xs i
    , AProject s ix t xs
    ) => AProject s ix ((Kon (->) :@: u) :@: t) xs where
  aproject q (i :: Index i) a =
    with @(ClosedInterpret u xs i) (aproject @s @ix @t @xs (q a) i)

instance AProject s ix t xs => AProject s ix (Kon ((->) a) :@: t) xs where
  aproject q i a = aproject @s @ix @t @xs (q a) i

instance (Functor f, AProject s ix t xs) => AProject s ix (Kon f :@: t) xs where
  aproject q i = fmap (aproject @s @ix @t @xs `flip` i) q

instance AProject s ix (Var (VZ :: TyVar (Type -> k) Type)) (Constant (a :: Type) :&&: xs) where
  aproject a _ = a

instance AProject s ix (Var v) xs => AProject s ix (Var (VS v :: TyVar (d -> k) Type)) ((x :: Choice d) :&&: xs) where
  aproject = aproject @s @ix @(Var v) @xs

instance Given s (WProject ix a)
  => AProject s ix (Var (VZ :: TyVar (Type -> k) Type)) (Choose a :&&: xs) where
  aproject = unWProject (given @s @(WProject ix a))

-- ALift Dict

instance ALift s ix (K.Eval t) xs where
  alift _ = error "TODO: Not yet implemented"

-- ALift1

instance
  ( ALift1 s ix v xs, forall (i :: ix). ClosedInterpret t xs i
  ) => ALift s ix (Var v :@: t) xs where
  alift p = alift1 @s @ix @v @xs @(Interpret t (Base xs))
    (with @(ClosedInterpret t xs i) p :: forall (i :: ix). Index i -> InterpretVar v (xs !? i) (Interpret t (Base xs)))

class ALift1 s ix v xs where
  alift1 :: forall a. (forall (i :: ix). Index i -> InterpretVar v (xs !? i) a) -> InterpretVar v (Base xs) a

instance ALift1 s ix v xs => ALift1 s ix (VS v :: TyVar (d -> l) (k -> Type)) ((x :: Choice d) :&&: xs) where
  alift1 = alift1 @s @ix @v @xs

instance Given s (WLift1 ix f)
  => ALift1 s ix (VZ :: TyVar ((k -> Type) -> l) (k -> Type)) (Choose (f :: k -> Type) :&&: xs) where
  alift1 = unWLift1 (given @s @(WLift1 ix f))

-- AProject1

instance
  ( AProject1 s ix v xs, forall (i :: ix). ClosedInterpret t xs i
  ) => AProject s ix (Var v :@: t) xs where
  aproject q (i :: Index i) =
    with @(ClosedInterpret t xs i)
      (aproject1 @s @ix @v @xs @(Interpret t (Base xs)) q i)

class AProject1 s ix v xs where
  aproject1 :: forall a (i :: ix). InterpretVar v (Base xs) a -> Index i -> InterpretVar v (xs !? i) a

instance AProject1 s ix v xs => AProject1 s ix (VS v :: TyVar (d -> l) (k -> Type)) ((x :: Choice d) :&&: xs) where
  aproject1 = aproject1 @s @ix @v @xs

instance Given s (WProject1 ix f)
  => AProject1 s ix (VZ :: TyVar ((k -> Type) -> l) (k -> Type)) (Choose (f :: k -> Type) :&&: xs) where
  aproject1 = unWProject1 (given @s @(WProject1 ix f))

newtype WProject ix a = WProject { unWProject :: Project ix a }
newtype WLift ix a = WLift { unWLift :: Lift ix a }

newtype WProject1 ix f = WProject1 { unWProject1 :: Project1 ix f }
newtype WLift1 ix f = WLift1 { unWLift1 :: Lift1 ix f }
