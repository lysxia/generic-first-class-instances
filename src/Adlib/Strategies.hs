{-# LANGUAGE
  AllowAmbiguousTypes,
  ConstraintKinds,
  DataKinds,
  FlexibleContexts,
  FlexibleInstances,
  MultiParamTypeClasses,
  PolyKinds,
  QuantifiedConstraints,
  RankNTypes,
  ScopedTypeVariables,
  TemplateHaskell,
  TypeApplications,
  TypeFamilies,
  TypeOperators,
  UndecidableInstances #-}
module Adlib.Strategies
  ( Derivable(..)
  , Via
  , deriveNewtype
  , Generically
  , Default
  , PartialDefault(..)
  , HasDict1
  , HasDictAt1

  , DictDerivable(..)
  , DictPartialDefault(..)
  ) where

import Adlib.CoerceDict.Class

import FCI (Dict, dict, mkDict)

import Data.PolyKinded (LoT(..), (:@@:))
import Generics.Kind.TH

import Data.Coerce
import Data.Function (fix)
import Data.Kind (Constraint, Type)

-- | Class of deriving strategies.
--
-- Usage:
--
-- @
-- 'FCI.instanceDict' [| 'derive' \@strategy :: 'Dict' c |]
-- @
--
-- Available strategies:
--
-- @
-- 'derive' \@('Via' a)     :: 'Dict' (c b)
-- 'derive' \@'Generically' :: 'Dict' c
-- 'derive' \@'Default'     :: 'Dict' c
-- @
--
-- Feel free to define your own strategies.
class Derivable (s :: Type) (c :: Constraint) where
  derive :: Dict c

-- | \"Via\" deriving strategy.
--
-- Example:
--
-- @
-- newtype T = T Int
--
-- 'FCI.instanceDict' [| 'derive' \@('Via' Int) :: 'Dict' ('Eq' T) |]
-- @
data Via (a :: k)

instance (CoerceDict (c a) (c b), c a) => Derivable (Via a) (c b) where
  derive = coerceDict (dict @(c a))

-- | Newtype deriving (aka.
-- [@GeneralizedNewtypeDeriving@](https://downloads.haskell.org/ghc/latest/docs/users_guide/exts/newtype_deriving.html))
-- is the special case of /deriving via/ where the 'Via' type is the underlying
-- type of the newtype.
--
-- 'deriveNewtype' must be applied to the newtype constructor, to infer the underlying type.
--
-- Example:
--
-- @
-- newtype T = T Int
--
-- 'FCI.instanceDict' [| 'deriveNewtype' T :: 'Dict' ('Eq' T) |]
-- @
deriveNewtype :: forall c a b. (Coercible a b, Derivable (Via a) (c b)) => (a -> b) -> Dict (c b)
deriveNewtype _ = derive @(Via a)

-- | Generic deriving strategy.
--
-- Example:
--
-- @
-- data T = A | B | C
--
-- 'FCI.instanceDict' [| 'derive' \@'Generically' :: 'Dict' ('Eq' T) |]
-- @
data Generically

-- | Default deriving strategy.
--
-- @
-- 'FCI.instanceDict' [| 'derive' \@'Default' :: 'Dict' ('Control.Exception.Exception' E) |]
-- @
data Default

-- | Partial default implementations.
-- Implements some methods in terms of other methods,
--
-- __Warning:__ there will be no warning if an implementation using
-- 'partialDefault' is incomplete, since it's just a record update.
--
-- Example: default implementation of 'Ord'.
--
-- Using 'partialDefault' in an 'Ord' instance.
--
-- @
-- data N = Z | S N
-- -- Eq T omitted
--
-- (.<=) :: N -> N -> Bool
-- Z .<= _ = 'True'
-- S _ .<= Z = 'False'
-- S m .<= S n = m .<= n
--
-- 'FCI.instanceDict' [| 'partialDefault' { ('Adlib.Instances.|<=') = (.<=) } :: Dict (Ord T) |]
-- @
--
-- Defining the t'PartialDefault' instance for 'Ord'.
-- 
-- @
-- instance 'Ord' a => t'PartialDefault' ('Ord' a) where
--   'partialDefault' = 'Adlib.Instances.Ord'
--     { 'Adlib.Instances._Eq' = 'dict' \@('Eq' a)
--     , 'Adlib.Instances._compare' = \\x y -> if x '==' y then 'EQ' else if x '<=' y then 'LT' else 'GT'
--     , ('Adlib.Instances.|<')  = \\x y -> 'compare' x y '==' 'LT'
--     , ('Adlib.Instances.|<=') = \\x y -> 'compare' x y '/=' 'GT'
--     , ('Adlib.Instances.|>')  = \\x y -> 'compare' x y '==' 'GT'
--     , ('Adlib.Instances.|>=') = \\x y -> 'compare' x y '/=' 'LT'
--     , 'Adlib.Instances._max'  = \\x y -> if x '<=' y then y else x
--     , 'Adlib.Instances._min'  = \\x y -> if x '<=' y then x else y
--     }
-- @
class PartialDefault (c :: Constraint) where
  partialDefault :: Dict c

class    (Dict (c t) ~ f t) => HasDictAt1 f c t
instance (Dict (c t) ~ f t) => HasDictAt1 f c t

class    (forall t. HasDictAt1 f c t) => HasDict1 f c
instance (forall t. HasDictAt1 f c t) => HasDict1 f c

mkDict ''Derivable
mkDict ''PartialDefault
-- The fcfify instance for Dict already exists in Adlib.Internal, so we ignore it here.
$(do
  _ <- preDeriveGenericK 'Derivable
  _ <- preDeriveGenericK 'PartialDefault
  postDeriveGenericK)
