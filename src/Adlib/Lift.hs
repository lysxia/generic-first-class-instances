{-# LANGUAGE
  AllowAmbiguousTypes,
  ConstraintKinds,
  DataKinds,
  FlexibleContexts,
  FlexibleInstances,
  ImpredicativeTypes,
  MultiParamTypeClasses,
  PolyKinds,
  QuantifiedConstraints,
  RankNTypes,
  ScopedTypeVariables,
  TypeApplications,
  TypeFamilies,
  UndecidableInstances #-}
{-# OPTIONS_GHC -fprint-explicit-kinds #-}

-- | Derive instances for products.
--
-- For a generic class @C@, implement @(C a, C b) => C (a, b)@.
--
-- Also generalize to arbitrary products (@()@, @(a, b, c)@, etc.),
-- and exponentials @(r -> a)@ (viewed as "@r@-ary" tuple).
--
-- Those are all special cases of the generalized product (dependent function type):
-- @forall (i :: I) -> F i@. The idea behind this implementation is that
-- many type constructors @D@ commute with products:
-- @D (forall i -> F i) = forall i -> D (F i)@.
module Adlib.Lift
  ( Lift
  , Project
  , glifted
  , glifted1
  , elevate
  , Given
  , G.WLift(..)
  , G.WProject(..)
  , DictGiven(..)

    -- * Indices
  , Index
  , type (!!)

    -- ** Indices instances
  , S2
  ) where

import Data.Kind (Constraint, Type)
import Generics.Kind (GenericK(..), LoT1)

import FCI (Dict)
import FCI.Base
import FCI.Unsafe ((==>))

import Adlib.Lift.Core
import qualified Adlib.Lift.Generic as G
import Adlib.Instances
import Adlib.Strategies
import Adlib.Internal

class G.GLift s ix (RepK d) (LoT1 (G.Choose a)) => GLiftRepDict s ix c d a
instance G.GLift s ix (RepK d) (LoT1 (G.Choose a)) => GLiftRepDict s ix c d a

type GLiftDict_ s ix c d a = (GenericK d, HasDict1 d c, GLiftRepDict s ix c d a) :: Constraint

class GLiftDict_ s ix c d a => GLiftDict s ix c d a
instance GLiftDict_ s ix c d a => GLiftDict s ix c d a

glift :: forall s ix c a d. GLiftDict s ix c d a => LiftDict ix c a
glift p = toK @_ @d @(LoT1 a) (G.glift @s @ix @(RepK d) @(LoT1 (G.Choose a)) (fromK @_ @d . p))

glifted :: forall ix c a d. GLifted ix c a d => Dict (c a)
glifted =
  Given @_ @Private (G.WLift (theLift @ix @a)) ==>
  Given @_ @Private (G.WProject (theProject @ix @a)) ==>
  elevate (glift @Private @ix @c @a @d)

-- | Constraint for 'glifted'.
type GLifted ix c a d =
  ( TheLift ix a
  , TheProject ix a
  , ForEach ix (Indexed c a)
  , GenericK d
  , HasDict1 d c
  , forall (s :: Type). (Given s (G.WLift ix a), Given s (G.WProject ix a)) => GLiftRepDict s ix c d a
  ) :: Constraint

glifted1 :: forall k ix c (f :: k -> Type) d. GLifted1 ix c f d => Dict (c f)
glifted1 =
  Given @_ @Private (G.WLift1 (theLift1 @ix @f)) ==>
  Given @_ @Private (G.WProject1 (theProject1 @ix @f)) ==>
  elevate (glift @Private @ix @c @f @d)

data Private  -- ^ A private scope

-- | Constraint for 'glifted1'.
type GLifted1 ix c f d =
  ( TheLift1 ix f
  , TheProject1 ix f
  , ForEach ix (Indexed c f)
  , GenericK d
  , HasDict1 d c
  , forall (s :: Type). (Given s (G.WLift1 ix f), Given s (G.WProject1 ix f)) => GLiftRepDict s ix c d f
  ) :: Constraint
