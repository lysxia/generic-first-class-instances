{-# LANGUAGE
  ConstraintKinds,
  DataKinds,
  FlexibleInstances,
  MultiParamTypeClasses,
  PolyKinds,
  RankNTypes,
  ScopedTypeVariables,
  TemplateHaskell,
  TypeFamilies,
  UndecidableInstances #-}

module Adlib.CoerceDict.Class
  ( CoerceDict(..)
  , DictCoerceDict(..)
  ) where

import FCI (Dict, mkDict)
import Generics.Kind.TH

import Data.Kind (Type)

import Adlib.Internal ()

-- | Constraint used by deriving @Via@.
--
-- Instances of t'CoerceDict' can be derived 'Adlib.Generically' for classes
-- that implement 'Generics.Kind.GenericK'. This enables 'Adlib.Via' deriving
-- for the same classes as those supported by GHC's native @DerivingVia@.
--
-- Instances can also be manually written to extend 'Adlib.Via' to classes with
-- methods that cannot be simply coerced.
--
-- === __Implementation notes__
--
-- 'coerce' alone is too restricted for higher-kinded classes like Functor.
-- For example,
--
-- @
-- newtype I a = I a
-- @
--
-- We don't have @Coercible I Identity@, even though we have @forall a.
-- Coercible (I a) (Identity a)@.
--
-- 'coerce' would also want to coerce the fields corresponding to superclasses,
-- but for deriving, we only care about method fields.
--
-- The generic implementation currently disambiguates superclass fields from
-- method fields from their types. You mustn't have a method with a type @FCI.Dict c@,
-- or surprising results may occur.
class CoerceDict c d where
  coerceDict :: Dict c -> Dict d

mkDict ''CoerceDict
-- The fcfify instance already exists in Adlib.Internal, so we ignore it here.
preDeriveGenericK 'CoerceDict *> postDeriveGenericK
