{-# LANGUAGE
  AllowAmbiguousTypes,
  ConstraintKinds,
  DataKinds,
  FlexibleContexts,
  FlexibleInstances,
  ImpredicativeTypes,
  MultiParamTypeClasses,
  PartialTypeSignatures,
  PolyKinds,
  QuantifiedConstraints,
  ScopedTypeVariables,
  StandaloneKindSignatures,
  TemplateHaskell,
  TypeApplications,
  TypeFamilies,
  TypeOperators,
  UndecidableInstances,
  UndecidableSuperClasses #-}

module Adlib.CoerceDict.Generic where

import Adlib.CoerceDict.Class
import Adlib.Internal (DictName, with)

import Fcf.Family
import FCI (Dict, dict)
import Generics.Kind

import Data.Coerce
import Data.Kind (Type, Constraint)
import Data.Proxy (Proxy(..))
import Data.Type.Equality (type (~~), (:~:)(..))

class GCoerce (Dict c) (Dict d) => GCoerceDict c d
instance  GCoerce (Dict c) (Dict d) => GCoerceDict c d

class GCoerce c d where
  gcoerce :: c -> d

gCoerceDict :: GCoerceDict c d => Dict (CoerceDict c d)
gCoerceDict = CoerceDict { _coerceDict = gcoerce }

-- Unapply: Find the head of the dictionary type

class 
  (c :@@: xs ~ d :@@: ys, c' :@@: xs' ~ d :@@: ys') =>
  Unapply (c :: k) (xs :: LoT k) (c' :: k) (xs' :: LoT k)
          (d :: l) (ys :: LoT l) (ys' :: LoT l)

instance
  Unapply c (a ':&&: xs) c' (a' ':&&: xs') d ys ys' =>
  Unapply (c a) xs (c' a') xs' d ys ys'

instance {-# OVERLAPPABLE #-}
  (c ~~ d, xs ~~ ys, c' ~~ d, xs' ~~ ys') => Unapply c xs c' xs' d ys ys'

-- Coerce generic RepK.

class CoerceRep f xs xs' where
  coerceRep :: f xs -> f xs'

-- Convert dictionary c ~ (d :@@: ys) to RepK (d :@@: ys)
instance (Unapply c LoT0 c' LoT0 d ys ys', GenericK d, CoerceRep (RepK d) ys ys') => GCoerce c c' where
  gcoerce = toK @_ @d . coerceRep @(RepK d) @ys @ys' . fromK @_ @d

instance CoerceRep f xs xs' => CoerceRep (M1 i c f) xs xs' where
  coerceRep (M1 f) = M1 (coerceRep f)

instance (CoerceRep f xs xs', CoerceRep g xs xs') => CoerceRep (f :*: g) xs xs' where
  coerceRep (f :*: g) = coerceRep f :*: coerceRep g

instance CoerceRep U1 xs xs' where
  coerceRep U1 = U1

class CoerceAtom t xs xs' where
  coerceAtom :: Interpret t xs -> Interpret t xs'

wrappedCoerceAtom :: forall t xs xs'. CoerceAtom t xs xs' => WrappedI t xs -> WrappedI t xs'
wrappedCoerceAtom (WrapI x) = WrapI (coerceAtom @t @xs @xs' x)

instance CoerceAtom t xs xs' => CoerceRep (Field t) xs xs' where
  coerceRep (Field t) = Field (coerceAtom @t @xs @xs' t)

instance (forall x. CoerceAtom t (x ':&&: xs) (x ':&&: xs')) => CoerceAtom ('ForAll t) xs xs' where
  coerceAtom t = fromWrappedI
    (wrappedCoerceAtom @t @(x ':&&: xs) @(x ':&&: xs') (toWrappedI t)
      :: forall x. WrappedI t (x ':&&: xs'))

class Interpret c xs => InterpretC c xs
instance Interpret c xs => InterpretC c xs

instance (Interpret c xs' => CoerceAtom t xs xs', Interpret c xs' => InterpretC c xs)
   => CoerceAtom (c ':=>>: t) xs xs' where
  coerceAtom (SuchThatI t) = SuchThatI (coerceAtom @t @xs @xs' (with @(InterpretC c xs) t))

-- Apply coerce to each field, under all quantifiers (ForAll, :=>>:)
instance {-# OVERLAPPABLE #-} (Coercible (Interpret t xs) (Interpret t xs'))
  => CoerceAtom t xs xs' where
  coerceAtom = coerce

-- For superclasses, we just require the coerced-to type to already be an instance.
-- This avoids propagating non-trivial constraints when the superclasses are complex.
--
-- For some reason the typechecker doesn't see that (Res DictName '() Any) is equal to to Type
-- so we have to use NDFamily_ instead of NDFamily and add this ugly constraint.
instance (Interpret c xs', Interpret ('Eval ('Kon (NDFamily_ DictName P0 e) ':@: (('Kon '(,) ':@: c) ':@: 'Kon '()))) xs' ~ Dict (Interpret c xs'))
  => CoerceAtom ('Eval ('Kon (NDFamily_ DictName P0 e) ':@: (('Kon '(,) ':@: c) ':@: 'Kon '()))) xs xs' where
  coerceAtom _ = dict @(Interpret c xs')
