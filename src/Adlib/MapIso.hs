{-# LANGUAGE
  FlexibleInstances,
  MultiParamTypeClasses,
  ScopedTypeVariables,
  TemplateHaskell,
  TypeApplications,
  TypeOperators,
  UndecidableInstances #-}
module Adlib.MapIso
  ( module MapIso
  , genericIso
  ) where

import Data.List.NonEmpty (NonEmpty)
import Text.Read (ReadPrec)

import Adlib.Strategies
import Adlib.MapIso.Class as MapIso
import Adlib.MapIso.Generic as MapIso (GMapIso, gmapIso, gMapIso)
import Adlib.Instances

import FCI (Dict, instanceDict)
import FCI.Base

import Generics.Kind

genericIso :: forall a x. GenericK a => (RepK a x <-> a)
genericIso = Iso toK fromK

instance GMapIso c => Derivable Generically (MapIso c) where
  derive = gMapIso

-- instance {-# INCOHERENT #-} Functor f => MapIso f where
--   mapIso = fmap . from

instance MapIso Maybe where mapIso = fmap . from
instance MapIso [] where mapIso = fmap . from
instance MapIso NonEmpty where mapIso = fmap . from
instance MapIso ReadPrec where mapIso = fmap . from

instanceDict [| derive @Generically :: Dict (MapIso DictEq) |]
instanceDict [| derive @Generically :: Dict (MapIso DictOrd) |]
instanceDict [| derive @Generically :: Dict (MapIso DictSemigroup) |]
instanceDict [| derive @Generically :: Dict (MapIso DictMonoid) |]
instanceDict [| derive @Generically :: Dict (MapIso DictShow) |]
instanceDict [| derive @Generically :: Dict (MapIso DictRead) |]
