{-# LANGUAGE
  DataKinds,
  ExplicitNamespaces,
  FlexibleInstances,
  KindSignatures,
  RankNTypes,
  TemplateHaskell,
  TypeFamilies,
  TypeOperators #-}
module Adlib.MapIso.Class
  ( type (<->)(..)
  , MapIso(..)
  , DictMapIso(..)
  ) where

import FCI (Dict, mkDict)

import Generics.Kind.TH

data a <-> b = Iso
  { from :: a -> b
  , to   :: b -> a
  }

class MapIso c where
  mapIso :: (a <-> b) -> c a -> c b

mkDict ''MapIso
deriveGenericK 'MapIso
