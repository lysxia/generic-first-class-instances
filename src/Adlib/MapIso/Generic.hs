{-# LANGUAGE
  ConstraintKinds,
  DataKinds,
  FlexibleContexts,
  FlexibleInstances,
  TypeOperators,
  MultiParamTypeClasses,
  PolyKinds,
  QuantifiedConstraints,
  ScopedTypeVariables,
  TypeApplications,
  TypeFamilies,
  UndecidableInstances #-}
module Adlib.MapIso.Generic where

import Adlib.Internal (with)
import Adlib.MapIso.Class

import FCI (Dict)

import Generics.Kind

import Data.Bifunctor

class    RepMapIso a b (RepK (c a)) (RepK (c b)) => RepMapIsoRepK a b c
instance RepMapIso a b (RepK (c a)) (RepK (c b)) => RepMapIsoRepK a b c

gmapIso :: forall c a b. GMapIso c => (a <-> b) -> (c a) -> (c b)
gmapIso e = toK @_ @(c b) . with @(RepMapIsoRepK a b c) (repmapIso e) . fromK @_ @(c a)

class    (forall a. GenericK (c a), forall a b. RepMapIsoRepK a b c) => GMapIso c
instance (forall a. GenericK (c a), forall a b. RepMapIsoRepK a b c) => GMapIso c

gMapIso :: GMapIso c => Dict (MapIso c)
gMapIso = MapIso gmapIso

class RepMapIso a b r s where
  repmapIso :: (a <-> b) -> r x -> s x

instance (RepMapIso a b r1 s1, RepMapIso a b r2 s2) =>
  RepMapIso a b (r1 :*: r2) (s1 :*: s2) where
  repmapIso e (r1 :*: r2) = repmapIso e r1 :*: repmapIso e r2

instance RepMapIso a b r s => RepMapIso a b (M1 i c r) (M1 i c s) where
  repmapIso e (M1 r) = M1 (repmapIso e r)

instance (forall x. IMapIsoInterpret a b r s x) => RepMapIso a b (Field r) (Field s) where
  repmapIso e (Field r :: Field r x) = Field (with @(IMapIsoInterpret a b r s x) (imapIso e r))

class    IMapIso a b (Interpret r x) (Interpret s x) => IMapIsoInterpret a b r s x
instance IMapIso a b (Interpret r x) (Interpret s x) => IMapIsoInterpret a b r s x

class IMapIso a b r s where
  imapIso :: (a <-> b) -> r -> s

instance {-# INCOHERENT #-} IMapIso a b a b where
  imapIso = from

instance {-# INCOHERENT #-} IMapIso a b b a where
  imapIso = to

instance {-# INCOHERENT #-} IMapIso a b r r where
  imapIso _ = id

-- instance MapIso c => IMapIso a b (Dict (c a)) (Dict (c b)) where
--   imapIso = mapIso

instance (IMapIso a b s1 r1, IMapIso a b r2 s2) => IMapIso a b (r1 -> r2) (s1 -> s2) where
  imapIso e f = imapIso e . f . imapIso e

instance {-# INCOHERENT #-} (MapIso f, IMapIso a b r s, IMapIso a b s r) => IMapIso a b (f r) (f s) where
  imapIso e = mapIso (Iso (imapIso e) (imapIso e))

instance {-# INCOHERENT #-} (Bifunctor f, IMapIso a b r1 s1, IMapIso a b r2 s2) =>
  IMapIso a b (f r1 r2) (f s1 s2) where
  imapIso e = bimap (imapIso e) (imapIso e)

instance (forall t u. t ~ u => IMapIso a b (WrappedI r (t ':&&: x)) (WrappedI s (u ':&&: x))) =>
  IMapIso a b (ForAllI r x) (ForAllI s x) where
  imapIso e w = fromWrappedI (imapIso e (toWrappedI w))

instance IMapIso a b (Interpret r x) (Interpret s x) => IMapIso a b (WrappedI r x) (WrappedI s x) where
  imapIso e = WrapI . imapIso e . unwrapI

instance (Interpret c x => IMapIsoInterpret a b r s x) => IMapIso a b (SuchThatI c r x) (SuchThatI c s x) where
  imapIso e (SuchThatI w) = SuchThatI (with @(IMapIsoInterpret a b r s x) (imapIso e w))
