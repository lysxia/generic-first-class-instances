{-# LANGUAGE
  CPP,
  DataKinds,
  FlexibleInstances,
  KindSignatures,
  PolyKinds,
  TemplateHaskell,
  RankNTypes,
  ScopedTypeVariables,
  TypeFamilies,
  UndecidableInstances #-}

-- | 'Generics.Kind.GenericK' instances for dictionary types of standard classes.
module Adlib.Instances () where

import FCI.Base

import Generics.Kind.TH

preDeriveGenericK 'Eq
preDeriveGenericK 'Ord
preDeriveGenericK 'Semigroup
preDeriveGenericK 'Monoid
preDeriveGenericK 'Show
preDeriveGenericK 'Read
preDeriveGenericK 'Enum
preDeriveGenericK 'Bounded
preDeriveGenericK 'Num
preDeriveGenericK 'Real
preDeriveGenericK 'Integral
preDeriveGenericK 'Fractional
preDeriveGenericK 'Floating
preDeriveGenericK 'RealFrac
preDeriveGenericK 'RealFloat
preDeriveGenericK 'Bits
preDeriveGenericK 'FiniteBits
preDeriveGenericK 'Ix
preDeriveGenericK 'Functor
preDeriveGenericK 'Contravariant
preDeriveGenericK 'Applicative
preDeriveGenericK 'Alternative
preDeriveGenericK 'Monad
preDeriveGenericK 'MonadFail
preDeriveGenericK 'MonadPlus
preDeriveGenericK 'MonadFix
preDeriveGenericK 'Foldable
preDeriveGenericK 'Traversable
#if !MIN_VERSION_base(4,18,0)
preDeriveGenericK 'Bifunctor
preDeriveGenericK 'Bifoldable
preDeriveGenericK 'Bitraversable
#endif
preDeriveGenericK 'Exception
preDeriveGenericK 'Category
preDeriveGenericK 'Arrow
preDeriveGenericK 'ArrowZero
preDeriveGenericK 'ArrowPlus
preDeriveGenericK 'ArrowChoice
preDeriveGenericK 'ArrowApply
preDeriveGenericK 'ArrowLoop
preDeriveGenericK 'Storable
postDeriveGenericK
